package com.decerto.recruitment.service;


import com.decerto.recruitment.exception.EmptyDatabaseTableException;
import com.decerto.recruitment.model.RandomNumber;
import com.decerto.recruitment.repository.RandomNumberRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.util.Random;

@Service
@Slf4j
public class ConnectRandomNumberService {

    private final RandomNumberRepository randomNumberRepository;

    public ConnectRandomNumberService(RandomNumberRepository randomNumberRepository) {
        this.randomNumberRepository = randomNumberRepository;
    }

    public Integer getRandomNumbersConnect() {
        try {
            Random randValue = new Random();
            int randNumber = randValue.nextInt(randomNumberRepository.findAll().size());
            RandomNumber randomNumber = randomNumberRepository.getOne(randNumber);
            return randomNumber.getRandnumber() + randNumber;
        }catch (Exception e){
            throw new EmptyDatabaseTableException();
        }
    }

}
