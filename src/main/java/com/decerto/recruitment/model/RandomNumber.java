package com.decerto.recruitment.model;


import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;

import javax.persistence.*;

@Entity
@Table(name = "randomnumber")
@AllArgsConstructor
@NoArgsConstructor
public class RandomNumber {

    private Integer id;
    private Integer randnumber;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "Id")
    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    @Column(name = "randnumber")
    public Integer getRandnumber() {
        return randnumber;
    }

    public void setRandnumber(Integer randnumber) {
        this.randnumber = randnumber;
    }
}
