package com.decerto.recruitment.repository;

import com.decerto.recruitment.model.RandomNumber;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface RandomNumberRepository extends JpaRepository<RandomNumber, Integer> {
}
