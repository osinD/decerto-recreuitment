package com.decerto.recruitment.exception;

public class EmptyDatabaseTableException extends IllegalArgumentException {

    public EmptyDatabaseTableException(){
        super("Database does not have any values");
    }
}
