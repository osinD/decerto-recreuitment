package com.decerto.recruitment.controller;

import com.decerto.recruitment.service.ConnectRandomNumberService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller("/")
@RequestMapping
@RequiredArgsConstructor
public class RandomNumberConnectController {

    private final ConnectRandomNumberService connectRandomNumberService;

    @GetMapping
    @ResponseBody
    public Integer getNumbersConnected(){
        return  connectRandomNumberService.getRandomNumbersConnect();
    }

}
