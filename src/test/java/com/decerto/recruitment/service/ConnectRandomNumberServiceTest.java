package com.decerto.recruitment.service;

import com.decerto.recruitment.exception.EmptyDatabaseTableException;
import com.decerto.recruitment.repository.RandomNumberRepository;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;


import java.util.ArrayList;

@RunWith(MockitoJUnitRunner.class)
public class ConnectRandomNumberServiceTest {

    @Mock
    private RandomNumberRepository repository;

    @InjectMocks
    private ConnectRandomNumberService connectRandomNumberService;


    @Test(expected = EmptyDatabaseTableException.class)
    public void shouldReturnExceptionInEmptyDatabaseList(){

        Mockito.when(repository.findAll()).thenReturn(new ArrayList<>());
        connectRandomNumberService.getRandomNumbersConnect();

    }


}
